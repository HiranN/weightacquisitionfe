import { Component } from '@angular/core';
import { WeightService } from './weight.service';
import { NgForm } from '@angular/forms';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'weight-acquisition-project';

  weightDetails:any = [];
  showData: boolean = false;

  constructor(private weightService:WeightService) {
    this.getWeightDetails();
  }
 

  getWeightDetails() {
    this.weightService.getWeights().subscribe(
      (resp) => {
        console.log(resp);
        this.weightDetails = resp;
        },
      (err) => {
        console.log(err);
        
      }  
    );
  }

  resetTable(){
    this.weightDetails = [];
  }
}





