import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class WeightService {

  constructor(private http: HttpClient) { }

  API = 'http://localhost:8080';
  
  

  public getWeights(){
    return this.http.get(this.API + '/customFetch')
  }
}
